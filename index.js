import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    Dimensions,
    TouchableHighlight,
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import Welcome from './src/Welcome';
import Market from './src/Market/Market';
import Products from './src/Market/Products';
import AboutProduct from './src/Market/AboutProduct'
import Basket from './src/Market/Basket';
const window = Dimensions.get('window');

const testMarket = StackNavigator({
    Welcome: { screen: Welcome },
    //--------------------------
    Market: {screen: Market},
    Products: {screen: Products},
    AboutProduct: {screen: AboutProduct},
    Basket: {screen: Basket}
    //--------------------------
});

AppRegistry.registerComponent('testMarket', () => testMarket);
