import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    TouchableOpacity,
} from 'react-native';
import { Button } from 'react-native-elements'
import marketStyle from './styles/marketStyle'


class Market extends React.Component {
    _onProductCategoryClickHandler = () => {
        this.props.navigation.navigate('Products');
    }

    static navigationOptions = {
        title: 'Магазин',
        headerStyle: {
            backgroundColor: '#ffffff'},
    };

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(['Книги', 'Фильмы'])
        };
    }

    render() {
        return (
            <View style={marketStyle.container}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data) =>  <Button onPress={this._onProductCategoryClickHandler} title={data} style={{marginTop: 10}} backgroundColor='#ffffff' color='black' borderRadius={2}></Button>}
                />
            </View>
        );
    }
}

export default Market;