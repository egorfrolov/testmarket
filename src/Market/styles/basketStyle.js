import { StyleSheet } from 'react-native';

const basketStyle = StyleSheet.create({
    container: {
        flex: 1,
    },

    productsCountText: {
        fontSize: 18,
        textAlign:'left',
        marginLeft: 10,
        marginTop: 10,
    },

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#000000',
    },

    separatorSecond: {
        marginTop: 10,
        height: 1.2,
        backgroundColor: '#f0f4f6',
    },

    productPrice: {
        marginTop: 10,
        marginBottom: 10,
        marginBottom: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },

    productRowContainer: {
        height: 55,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },

    productName: {
        fontSize: 18,
        textAlign: 'left',
        textAlignVertical: 'center',
    },

    productImage: {
        width: 200*0.5,
        height: 312*0.5
    },

    endSum: {
        fontSize: 24,
        textAlign: 'left',
        marginTop: 10,
        marginBottom: 20,
        marginLeft: 20,
        fontWeight: 'bold',
    },

    productButton: {
        textAlign: 'center'
    }

});

export default basketStyle;