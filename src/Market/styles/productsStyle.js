import { StyleSheet } from 'react-native';

const productsStyle = StyleSheet.create({
    container: {
        flex: 1,
    },

    cardContainer: {

    },

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#f0f4f6',
    },

    separatorSecond: {
        marginTop: 10,
        height: 1.2,
        backgroundColor: '#f0f4f6',
    },

    productPrice: {
        marginTop: 10,
        marginBottom: 10,
        marginBottom: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },

    productRowContainer: {
        height: 55,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },

    productName: {
        fontSize: 18,
        textAlign: 'left',
        textAlignVertical: 'center',
    },

    productImage: {
        width: 200*0.5,
        height: 312*0.5
    },



    productButton: {
        textAlign: 'center'
    }

});

export default productsStyle;