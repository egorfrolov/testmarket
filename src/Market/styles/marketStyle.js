import { StyleSheet } from 'react-native';

const marketStyle = StyleSheet.create({
    container: {
        flex: 1,
    },
    marketButtonZone: {
        flex: 1
    },

    mainMenuRowButton: {
        height: 35,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },

    marketButton: {
        width: window.width,
        height: window.height * 0.3
    },

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#f0f4f6',
    },

    mainMenuRowButtonText: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },

    titleChooseCategoryText: {
        fontSize: 24,
        fontWeight: 'bold'
    },

    categoryRowContainer: {
        height: 55,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },

    categoryText: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
});

export default marketStyle;