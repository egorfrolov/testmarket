import { StyleSheet } from 'react-native';

const aboutProductStyle = StyleSheet.create({
    container: {
        flex: 1,
    },


    separator: {
        marginTop: 10,
        height: 1.2,
        backgroundColor: '#f0f4f6',
    },

    productName: {
        fontSize: 24,
        textAlign: 'center',
        textAlignVertical: 'center',
    },

    productDescription: {
      fontSize: 14,
        marginTop: 8,
    },

    productImage: {
        marginRight: 1,
        width: 200,
        height: 312
    },

    productButton: {
        textAlign: 'center'
    }

});

export default aboutProductStyle;