import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import aboutProductStyle from './styles/aboutProductStyle'
import { Card, Button } from 'react-native-elements'
//import { StackNavigator } from 'react-navigation';
//import TouchableItem from '/node_modules/react-navigation/lib-rn/views/TouchableItem';


class AboutProduct extends React.Component {
    _onPutInBasketClickHandler= () => {
        this.putInBasketFlag = true;
        this.props.navigation.state.params.rrr.productsInBacket.push(this.props.navigation.state.params.index);
        this.props.navigation.state.params.rrr.inBackerCount++;
        this.props.navigation.state.params.rrr.setState({textVal: 'Корзина\t(' + this.props.navigation.state.params.rrr.inBackerCount + ')'});
    }
    getName(){
       return  this.props.navigation.state.params.data.name;
    }
    static navigationOptions = {
        title: 'Подробнее',
        headerStyle: {
            backgroundColor: '#ffffff'},
    };

    constructor(props) {
        super(props);
        this.putInBasketFlag = false;
        //alert(this.props.navigation.state.params.name);
    }

    render() {
        let productName = this.props.navigation.state.params.data.name;
        let productDescription = this.props.navigation.state.params.data.description;
        let productImage = this.props.navigation.state.params.data.image;
        return (
            <ScrollView>
            <View style={aboutProductStyle.container}>
                <Card
                    title={productName}>
                    <Image style={aboutProductStyle.productImage} source={productImage}/>
                    <View style={aboutProductStyle.separator} />
                <Text style={{marginTop: 12, fontSize: 18, textAlign: 'left'}}>Описание</Text>
                <Text style={aboutProductStyle.productDescription}>{productDescription}</Text>
                    <View style={aboutProductStyle.separator} />
                <Button
                    onPress={this._onPutInBasketClickHandler}
                    backgroundColor='#03A9F4'
                    buttonStyle={{marginTop: 20, borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                    title='В корзину' />
                </Card>
            </View>
            </ScrollView>
        );
    }
}

export default AboutProduct;