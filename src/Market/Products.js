import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    TouchableOpacity,
} from 'react-native';

import productStyle from './styles/productsStyle'
import { Card, Button, Badge } from 'react-native-elements'


class Products extends React.Component {

    _onProductClickHandler = (temp, productIndex) => {
        this.props.navigation.navigate('AboutProduct', {data: temp, index: productIndex,rrr: this} );
    }

    _onBasketClickHandler = () => {
        this.props.navigation.navigate('Basket', {positions: this.productsInBacket, data: this.productsList,count: this.inBackerCount});
    }

    _onPutInBasketClickHandler = (productIndex) => {
        this.productsInBacket.push(productIndex);
        this.inBackerCount++;
        this.setState({textVal: 'Корзина\t(' + this.inBackerCount + ')'});
    }

    getProductsList() {
        var pList = new Array();
        pList = [{name: 'Алые паруса', price: '500',briefly: 'Повесть-феерия Александра Грина о непоколебимой вере и всепобеждающей, возвышенной мечте, о том, что каждый может сделать для близкого чудо.', description: 'Повесть-феерия Александра Грина о непоколебимой вере и всепобеждающей, возвышенной мечте, о том, что каждый может сделать для близкого чудо.',image: require('../../content/images/aleksandr_grin__alye_parusa.png')}, {name: 'Записки охотника', price: '600', briefly: 'Cборник рассказов Ивана Сергеевича Тургенева, печатавшихся в 1847—1851 годах в журнале «Современник» и выпущенных отдельным изданием в 1852 году.', description: 'Cборник рассказов Ивана Сергеевича Тургенева, печатавшихся в 1847—1851 годах в журнале «Современник» и выпущенных отдельным изданием в 1852 году.',image: require('../../content/images/product_image_default.png')}];
        return pList;
    }

    static navigationOptions = {
        title: 'Продукты',
        headerStyle: {
            backgroundColor: '#ffffff'},

    };

    constructor(props) {
        super(props);
        this.productsInBacket = new Array();
        this.inBackerCount = 0;
        this.productsList = this.getProductsList();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.productsList),
            textVal: 'Корзина\t(' + this.inBackerCount + ')'
        };
    }

    render() {
        return (
            <View style={productStyle.container}>
                <Button
                    onPress={this._onBasketClickHandler}
                    backgroundColor='#03A9F4'
                    raised
                    Action
                    title={this.state.textVal} />
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data, _, rowID) =>
                        <Card
                            title={data.name}
                            image={data.image}>
                            <Text style={{marginBottom: 10}}>
                                {data.briefly}
                            </Text>
                            <View style={productStyle.separatorSecond} />
                            <Text style={productStyle.productPrice}>Цена: {data.price} руб.</Text>
                            <Button
                                onPress={() => this._onProductClickHandler(data, rowID)}
                                backgroundColor="#e9e9ef"
                                color="#000000"
                                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 10}}
                                title='Подробнее' />
                            <Button
                                onPress={() => this._onPutInBasketClickHandler(rowID)}
                                backgroundColor='#03A9F4'
                                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                                title='В корзину' />
                        </Card>}
                />
            </View>
        );
    }
}

export default Products;