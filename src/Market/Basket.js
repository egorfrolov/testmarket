import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import basketStyle from './styles/basketStyle'
import { Card, Button } from 'react-native-elements'


class Basket extends React.Component {

    static navigationOptions = {
        title: 'Корзина',
        headerStyle: {
            backgroundColor: '#ffffff'},
    };

    countProductInList(list, product) {
        var counter = 0;
        for (var i = 0; i < list.length; i++) {
            if (list[i].name == product) {
                counter++;
            }
        }
        return counter;
    }

    getBasketPositions() {
        var prods = new Array();
        for (var i = 0; i < this.props.navigation.state.params.positions.length; i++) {
            prods.push(this.props.navigation.state.params.data[this.props.navigation.state.params.positions[i]]);
        }
        return prods;
    }

    getEndSum() {
        var sum = 0;
        for (var i = 0; i < this.productsInBacket.length; i++) {
            sum += parseInt(this.productsInBacket[i].price);
        }
        return sum;
    }

    constructor(props) {
        super(props);

        this.productsInBacket = this.getBasketPositions();
        this.inBackerCount = 0;
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.productsInBacket),
            count: this.props.navigation.state.params.count == 0 ? 'Нет позиций в корзине' : 'Кол-во товаров:\t' + this.props.navigation.state.params.count,
            endSum: this.getEndSum()
        };
    }


    render() {
        let productCount = this.props.navigation.state.params.count;
        return (
            <View style={basketStyle.container}>
                <Text style={basketStyle.productsCountText}>{this.state.count}</Text>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data, _, rowID) =>
                        <Card
                            title={data.name}>
                            <Text style={basketStyle.productPrice}>Цена: {data.price} руб.</Text>
                        </Card>}
                />
                <View style={basketStyle.separator}/>
                <Text style={basketStyle.endSum}>Итого: {this.state.endSum}</Text>
            </View>
        );
    }
}

export default Basket;