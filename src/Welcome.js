import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
} from 'react-native';
import { Button } from 'react-native-elements'
import Market from './Market/Market';
import welcomeStyle from './welcomeStyle'
const window = Dimensions.get('window');

class Welcome extends Component {
    _onMarketClickHandler = () => {                      //here use arrow  function
        this.props.navigation.navigate('Market');
    }

    _onNewsClickHandler = () => {                      //here use arrow  function
        this.props.navigation.navigate('News');
    }

    static navigationOptions = {
        title: 'Главное меню',
        headerStyle: {
            backgroundColor: '#ffffff'},
    };

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={welcomeStyle.container}>
                <Button onPress={this._onMarketClickHandler} style={{marginTop: 10}} backgroundColor='#006fff' fontWeight='bold' borderRadius={2} title="Магазин" >
                </Button>
                <View style={{marginTop: 5}}/>
                <Button onPress={this._onNewsClickHandler} style={{marginTop: 10}} backgroundColor="#fec130" fontWeight='bold' borderRadius={2} title="Новости">
                </Button>
            </View>
        );
    }
};

export default Welcome;