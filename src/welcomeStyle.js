import { StyleSheet } from 'react-native';

const welcomeStyle = StyleSheet.create({
    titleStyle: {
        backgroundColor: '#ff2a0a'
    },
    container: {
        flex: 1,
    },
    mainMenuRowButton: {
        marginTop: 10,
        height: 55,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
    mainMenuRowButtonText: {
        fontSize: 18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#f0f4f6',
    },

});

export default welcomeStyle;