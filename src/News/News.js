import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ListView,
    TouchableOpacity,
} from 'react-native';
import { Card, Button, Badge } from 'react-native-elements'

import newsStyle from './styles/newsStyle'


class News extends React.Component {

    _onNewClickHandler = () => {
        alert("Довольно подробно");
    }

    static navigationOptions = {
        title: 'Новости',
        headerStyle: {
            backgroundColor: '#ffffff'},
    };

    getNewsList() {
        var pList = new Array();
        pList = [{title: 'Осеннаяя распродажа!', date: '02.09.2017',image: require('../../content/images/autumn_sale_2017.png'), description: 'Наш магазин представляет вашему вниманию скидки до 60% на множество книг и фильмов!'}, {title: 'В ближайшее время ожидаются теплые осенние дни', date: '06.09.2017',image: require('../../content/images/weather_06_09_2017.png'), description: 'Прекрасные солнечные дни ожидают нас с 6-го по 9-е сентября!'}];
        return pList;
    }

    constructor(props) {
        super(props);
        this.newsList = this.getNewsList();
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(this.newsList),
        };
    }



    render() {
        return (
            <View style={newsStyle.container}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data, _, rowID) =>
                        <Card
                            title={data.title}
                            image={data.image}>
                            <Text style={newsStyle.dateText}>{data.date}</Text>
                            <View style={newsStyle.separator} />
                            <Text style={{marginBottom: 20}}>
                                {data.description}
                            </Text>
                            <Button
                                onPress={() => this._onNewClickHandler(data, rowID)}
                                backgroundColor="#e9e9ef"
                                color="#000000"
                                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 10}}
                                title='Подробнее' />
                        </Card>}
                />
            </View>
        );
    }
}

export default News;