import { StyleSheet } from 'react-native';

const basketStyle = StyleSheet.create({
    container: {
        flex: 1,
    },

    productsCountText: {
        fontSize: 18,
        textAlign:'left',
        marginLeft: 10,
        marginTop: 10,
    },

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#f0f4f6',
        marginBottom: 20,
    },

    dateText: {
      fontSize: 11,
        marginLeft: 5,
        color: '#373737',
    },

});

export default basketStyle;